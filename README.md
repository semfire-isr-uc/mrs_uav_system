## Introduction (by Ali Ahmadi - v0.1)
This package is the main package for installation of MRS UAV system that contains the easy installation script.
For more information about the MRS UAV system, installation, and tutorials, you can read [this tutorial](https://www.overleaf.com/read/hrfkbymwjwyk)

### Modifications
This installation script has been modified in order to be compatible with SEMFIRE's bitbucket private repositories. So, it is essential to use this installation in order to have the modifications inside the MRS UAV system.

## Installation

Following options are provided, depending on who you are and what you want to do with the platform.
**Beware**, installing the system into a virtual machine may not be a good idea for you, unless you have a very powerfull computer.
Real-time simulations in Gazebo perform very poorly in virtual machines.


### "I have fresh Ubuntu install and want it quick and easy"

In this case we provide installation scripts that set everything up for you.
Our automated installation will:

* install Robot Operating System (ROS),
* install other dependencies such *git*, *gitman*,
* clone [uav_core](https://github.com/ctu-mrs/uav_core), [simulation](https://bitbucket.org/semfire-isr-uc/simulation.git), [example_ros_packages](https://github.com/ctu-mrs/example_ros_packages) into *~/git*,
* install more dependencies such as *tmux* and *tmuxinator*
* create our ros workspace in ```~/mrs_workspace``` for the *uav_core* and *simulation*,
* create a ros workspace in ```~/workspace``` for *examples*,
* link our packages to the workspaces,
* compile the workspaces,
* added configuration lines into your *~/.bashrc*.

To start the automatic installation, please paste the following code into your terminal and press **enter**.
You might be prompted a few times to confirm something by pressing enter:

```bash
cd /tmp
echo '
GIT_PATH=~/git
mkdir -p $GIT_PATH
cd $GIT_PATH
sudo apt-get -y install git
git clone https://bitbucket.org/semfire-isr-uc/mrs_uav_system/
cd mrs_uav_system
git checkout master
git pull
./install.sh -g $GIT_PATH
source ~/.bashrc' > clone.sh && source clone.sh
```

### "I already have ROS and just want to peek in"

If you already have ROS installed and if you are fluent with *workspaces*, *.bashrc*, *catkin tools*, etc., feel free to clone our repositories individually.
The [uav_core](https://github.com/ctu-mrs/uav_core) repository integrates our UAV control system.
Please follow its README for further instructions on how to install its particular dependencies.

The [simulation](https://bitbucket.org/semfire-isr-uc/simulation.git) repository provides resources for *Gazebo/ROS* simulation, including px4 Simulation-in-the-Loop (SITL), UAV models and useful sensor plugins.
Please follow its README for further instructions on how to install prerequisities.

For help with using the system, you can also refer to the [MRS Cheatsheet](https://ctu-mrs.github.io/docs/introduction/cheatsheet.html).

## Running the simulation
If you have successfully installed the system, you can continue with [starting the simulation](https://ctu-mrs.github.io/docs/simulation/howto.html).
